import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public toggleClick(sidenav) {
    if (!sidenav._isOpened) {
      sidenav.open();
    } else {
      sidenav.close();
    }
  }
}
