import { Component, OnInit } from '@angular/core';
import { MdDialog } from '@angular/material';
import { UserCreationDialog } from '../user-creation-dialog/user-creation-dialog';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  public users: Array<any> = [];
  constructor(
    private _dialog: MdDialog
  ) { }

  ngOnInit() {
    let users = localStorage.getItem('user');
    if (users) {
      this.users = JSON.parse(users);
    } else {
      this.users = [];
    }
  }

  /**
   * To create new user
   */
  public onCreateUserClick() {
    let dialogRef = this._dialog.open(UserCreationDialog, {
      width: '800px',
      height: '600px'
    });
    dialogRef.afterClosed().subscribe(result => {
      this.users.push(result);
      localStorage.setItem('user', JSON.stringify(this.users));
      console.log(result);
    });
  }

  /**
   * To edit the user details
   * @param user - currently selected user 
   */
  public onEditUserClick(user, index) {
    let dialogRef = this._dialog.open(UserCreationDialog, {
      width: '800px',
      height: '600px',
      disableClose: true
    });
    dialogRef.componentInstance.user = user;
    dialogRef.afterClosed().subscribe(result => {
      this.users[index] = result;
      localStorage.setItem('user', JSON.stringify(this.users));
      console.log(result);
    });
  }

  /**
   * To clear the user details
   */
  public onClearUserClick() {
    localStorage.clear();
    this.users = [];
  }

}
