import { BrowserModule } from '@angular/platform-browser';
import { FormsModule ,ReactiveFormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
  MdButtonModule,
  MdCheckboxModule,
  MdInputModule,
  MdSidenavModule,
  MdToolbarModule,
  MdIconModule,
  MdNativeDateModule,
  MdDatepickerModule,
  MdSelectModule,
  MdTabsModule,
  MaterialModule
} from '@angular/material';

import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import {UserCreationDialog} from './user-creation-dialog/user-creation-dialog';

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    UserCreationDialog
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MdButtonModule,
    MdCheckboxModule,
    MdInputModule,
    MdSidenavModule,
    MdIconModule,
    MaterialModule,
    BrowserAnimationsModule,
    ReactiveFormsModule
  ],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  entryComponents:[UserCreationDialog],
  bootstrap: [AppComponent]
})
export class AppModule { }
