import { Routes } from '@angular/router';
import { AppComponent } from './app.component';

export const APP_ROUTES: Routes = [
    {
        path: '',
        redirectTo: 'user-management',
        pathMatch:'full'
    },
     {
        path: 'user-management',
        component:AppComponent
    }
];
