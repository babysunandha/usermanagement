import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MdDialogRef } from '@angular/material';

@Component({
    selector: 'user-creation',
    templateUrl: './user-creation-dialog.html',
    styleUrls: ['./user-creation-dialog.scss']
})
export class UserCreationDialog implements OnInit {
    public userForm: FormGroup;
    public user: any = {};
    constructor(
        private fb: FormBuilder,
        public dialogRef: MdDialogRef<UserCreationDialog>)
    { }

    ngOnInit() {
        this.userForm = this.fb.group({
            userId: [this.user.userId ? this.user.userId : '', [Validators.required]],
            userName: [this.user.userName ? this.user.userName : '', [Validators.required]],
            userAge: [this.user.userAge ? this.user.userAge : '', [Validators.required]],
            userAddress: [this.user.userAddress ? this.user.userAddress : '', [Validators.required]]
        });
    }


    public onCreateClick() {
        this.dialogRef.close(this.userForm.value);
    }
}